// Практичне завдання
/*Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.
1. Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
2. Считать с помощью модального окна браузера число, которое введет пользователь (n).
3. С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
4. Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).*/

let numberFirst;
let numberSecond;
let numberInFibonacci;
do {
  numberFirst = prompt("Enter the first number:", numberFirst);
  numberSecond = prompt("Enter the second number:", numberSecond);
  numberInFibonacci = prompt(
    "Enter the third number - the serial number:",
    numberInFibonacci
  );
} while (
  isNaN(+numberFirst) ||
  +numberFirst === 0 ||
  isNaN(+numberSecond) ||
  +numberSecond === 0 ||
  isNaN(+numberInFibonacci) ||
  +numberInFibonacci === 0
);

function getNumberFibonacci(a, b, n) {
  for (let i = 0; i < n - 1; i++) {
    let temp = b;
    b = a + b;
    a = temp;
  }
  return a;
}
let result = getNumberFibonacci(
  +numberFirst,
  +numberSecond,
  +numberInFibonacci
);
alert(
  `The number of the Fibonacci sequence under the serial number ${numberInFibonacci} is: ${result}`
);
console.log(
  `The number of the Fibonacci sequence under the serial number ${numberInFibonacci} is: ${result}`
);
